﻿using UnityEngine;
using RedRain.Poplab.View.Onboarding;
using System;
using UnityEngine.UI;
using TMPro;


namespace RedRain.Poplab.OpeningOnboard
{

    public class OnBoardLoadingView : MonoBehaviour, ILoadingScreenView
    {
        [SerializeField] private TMP_Text m_LoadingText;
        [SerializeField] private TMP_Text m_LoadingValueText;
        [SerializeField] private Image m_Bar;
        [SerializeField] private GameObject m_BarBg;


        public void HidePage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(false);
        }


        public void ShowPage(Action onTransitionComplete = null)
        {
            this.m_LoadingText.text = "Loading...";
            this.m_Bar.fillAmount = 0;
            this.m_BarBg.SetActive(false);
            this.m_LoadingValueText.text = "";
            this.gameObject.SetActive(true);
        }


        public void UpdateProgress(float progress, string message = null)
        {
            if(!string.IsNullOrEmpty(message)) this.m_LoadingText.text = message;
            else this.m_LoadingText.text = "Loading...";

            if(!this.m_BarBg.activeSelf) this.m_BarBg.SetActive(progress > 0);
            this.m_Bar.fillAmount = progress;
            this.m_LoadingValueText.text = progress.ToString("00") + "%";

            if(!this.gameObject.activeSelf) this.gameObject.SetActive(true);
        }

    }

}