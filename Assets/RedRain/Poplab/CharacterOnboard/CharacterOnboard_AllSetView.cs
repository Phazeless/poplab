using System;
using RedRain.Poplab.View.Onboarding;
using UnityEngine;
using UnityEngine.UI;


namespace RedRain.Poplab.CharacterOnboard
{

    public class CharacterOnboard_AllSetView : MonoBehaviour, ICharacterAllSetView
    {
        [SerializeField] private Button m_btnBack;
        [SerializeField] private Button m_btnLaunch;

        public event Action onClickBack;
        public event Action onClickLaunch;


        private void Start()
        {
            m_btnBack.onClick.AddListener(delegate { onClickBack?.Invoke(); });
            m_btnLaunch.onClick.AddListener(delegate { onClickLaunch?.Invoke(); });
        }


        public void ShowPage(Action onTransitionComplete = null)
        {
            if(this && this.gameObject)
                this.gameObject.SetActive(true);

            onTransitionComplete?.Invoke();
        }


        public void HidePage(Action onTransitionComplete = null)
        {
            if(this && this.gameObject)
                this.gameObject.SetActive(false);

            onTransitionComplete?.Invoke();
        }
    }

}