using System;
using RedRain.Poplab.View.Onboarding;
using UnityEngine;
using UnityEngine.UI;


namespace RedRain.Poplab.CharacterOnboard
{

    public class CharacterOnboard_CustomiseView : MonoBehaviour, ICharacterCustomiseView
    {
        [SerializeField] private Button m_btnBack;

        public event Action onClickBack;


        private void Start()
        {
            this.m_btnBack.onClick.AddListener(delegate { onClickBack?.Invoke(); });
        }


        public void ShowPage(Action onTransitionComplete = null)
        {
            if(this && this.gameObject)
                this.gameObject.SetActive(true);

            onTransitionComplete?.Invoke();
        }


        public void HidePage(Action onTransitionComplete = null)
        {
            if(this && this.gameObject)
                this.gameObject.SetActive(false);

            onTransitionComplete?.Invoke();
        }
    }

}