// using System;
// using RedRain.Poplab.View.Onboarding;
// using UnityEngine;
// using UnityEngine.UI;
//
//
// namespace RedRain.Poplab.CharacterOnboard {
//     public class CharacterOnboard_Selection : MonoBehaviour, ICharacterSelectionView{
//         public static CharacterOnboard_Selection m_Instance;
//         public GameObject m_NamingPanel;
//
//         public Image m_FemaleCharacter;
//         public Image m_MaleCharacter;
//
//         public Color m_Select;
//         public Color m_Unselect;
//
//         public Button m_NextButton;
//
//         public int m_CurrentSelected;
//
//         public int activeIndex => this.m_CurrentSelected;
//
//         public event Action<int> onSelectSkin;
//         public event Action onClickNext;
//
//         void Start() {
//             if (m_Instance == null) m_Instance = this;
//             this.m_CurrentSelected = -1;
//         }
//
//         public void f_SelectFemale() {
//             this.m_FemaleCharacter.color = this.m_Select;
//             this.m_MaleCharacter.color = this.m_Unselect;
//             this.m_CurrentSelected = 0;
//             this.onSelectSkin?.Invoke(this.m_CurrentSelected);
//             this.m_NextButton.interactable = true;
//         }
//
//         public void f_SelectMale() {
//             this.m_FemaleCharacter.color = this.m_Unselect;
//             this.m_MaleCharacter.color = this.m_Select;
//             this.m_CurrentSelected = 1;
//             this.onSelectSkin?.Invoke(this.m_CurrentSelected);
//             this.m_NextButton.interactable = true;
//         }
//
//         public void f_ClearSelection() {
//             this.m_MaleCharacter.color = this.m_Unselect;
//             this.m_FemaleCharacter.color = this.m_Unselect;
//             this.m_CurrentSelected = -1;
//             this.onSelectSkin?.Invoke(this.m_CurrentSelected);
//             this.m_NextButton.interactable = false;
//         }
//
//         public void f_Proceed() {
//             this.onClickNext?.Invoke();
//             this.m_NamingPanel.SetActive(true);
//             CharacterOnboard_Username.m_Instance.m_SelectionPanel.SetActive(false);
//         }
//
//         public void HighlightSkin(int index) {
//             if (index == -1) this.f_ClearSelection();
//             else if (index == 0) this.f_SelectFemale();
//             else this.f_SelectMale();
//         }
//
//         public void ShowPage(Action onTransitionComplete = null) {
//             CharacterOnboard_Username.m_Instance.m_SelectionPanel.SetActive(true);
//             onTransitionComplete?.Invoke();
//         }
//
//         public void HidePage(Action onTransitionComplete = null) {
//             CharacterOnboard_Username.m_Instance.m_SelectionPanel.SetActive(false);
//             onTransitionComplete?.Invoke();
//         }
//     }
//
// }
