using System;
using RedRain.Poplab.View.Onboarding;
using UnityEngine;
using UnityEngine.UI;


namespace RedRain.Poplab.CharacterOnboard
{

    public class CharacterOnboard_SelectionView : MonoBehaviour, ICharacterSelectionView
    {
        //[SerializeField] GameObject m_NamingPanel;

        [SerializeField] Button m_FemaleCharacterButton;
        [SerializeField] Button m_MaleCharacterButton;

        [SerializeField] Image m_FemaleCharacter;
        [SerializeField] Image m_MaleCharacter;

        [SerializeField] Color m_Select;
        [SerializeField] Color m_Unselect;

        [SerializeField] Button m_NextButton;


        public event Action<int> onSelectSkin;
        public event Action onClickNext;
        public int activeIndex { get; private set; } = -2;


        private void Start()
        {
            this.ClearSelection();
            m_FemaleCharacterButton.onClick.AddListener(delegate { onSelectSkin?.Invoke(0); });
            m_MaleCharacterButton.onClick.AddListener(delegate { onSelectSkin?.Invoke(1); });
            m_NextButton.onClick.AddListener(delegate { onClickNext?.Invoke(); });
        }


        public void ShowPage(Action onTransitionComplete = null)
        {
            if(this && this.gameObject)
                this.gameObject.SetActive(true);

            onTransitionComplete?.Invoke();
        }


        public void HidePage(Action onTransitionComplete = null)
        {
            if(this && this.gameObject)
                this.gameObject.SetActive(false);

            onTransitionComplete?.Invoke();
        }


        /// <param name="index">0 = female, 1 = male, any other value = deselect</param>
        public void HighlightSkin(int index)
        {
            if(activeIndex == index) return;

            this.activeIndex = index;

            m_FemaleCharacter.color = index == 0 ? this.m_Select : this.m_Unselect;
            m_MaleCharacter.color = index == 1 ? this.m_Select : m_Unselect;

            this.onSelectSkin?.Invoke(index);
        }


        public void SetNextButtonInteractable(bool isInteractable)
        {
            m_NextButton.interactable = isInteractable;
        }


        public void ClearSelection() => HighlightSkin(-1);
    }

}