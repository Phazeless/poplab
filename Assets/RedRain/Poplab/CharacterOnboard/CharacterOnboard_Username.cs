// using System;
// using RedRain.Poplab.View.Onboarding;
// using TMPro;
// using UnityEngine;
//
//
// namespace RedRain.Poplab.CharacterOnboard {
//     public class CharacterOnboard_Username : MonoBehaviour, ICharacterNameView {
//         public static CharacterOnboard_Username m_Instance;
//
//         public GameObject m_SelectionPanel;
//         public GameObject m_UsernameWarning;
//         public TextMeshProUGUI m_WarningText;
//         public TMP_InputField m_InputField;
//
//         public GameObject m_NextButton;
//
//         public int activeIndex => throw new NotImplementedException();
//
//         public event Action<string> onInputChanged;
//         public event Action onClickBack;
//         public event Action onClickNext;
//
//
//         // Start is called before the first frame update
//         void Start() {
//             if (m_Instance == null) m_Instance = this;
//         }
//
//         // Update is called once per frame
//         void Update() {
//
//         }
//
//         public void f_Back() {
//             this.onClickBack?.Invoke();
//             this.m_SelectionPanel.SetActive(true);
//             CharacterOnboard_Selection.m_Instance.m_NamingPanel.SetActive(false);
//             CharacterOnboard_Selection.m_Instance.f_ClearSelection();
//         }
//         public void f_Next() {
//             this.onClickNext?.Invoke();
//             CharacterOnboard_Selection.m_Instance.m_NamingPanel.SetActive(false);
//         }
//
//         public void f_WarningError(string p_ErrorText) {
//             this.m_UsernameWarning.SetActive(true);
//             this.m_WarningText.text = p_ErrorText;
//         }
//
//         public void f_ReEnterUsername() {
//             this.m_UsernameWarning.SetActive(false);
//             this.m_InputField.text = "";
//             this.m_InputField.Select();
//         }
//
//         public void f_SubmitUsername() { //PUT YOUR ACTUAL VALDIATION HERE
//             //DUMMY TEST
//             //IF FALSE
//             this.f_WarningError("Whop! Your name is taken");
//             //IF TRUE m_NextButton.interactable = true;
//         }
//
//         public void f_OnValueChanged(string p_Value) {
//             this.onInputChanged?.Invoke(p_Value);
//         }
//
//         public void ShowRegexErrors(string[] errors = null) {
//             string Concat = "";
//             if (errors != null && errors.Length > 0) {
//                 for (int i = 0; i < errors.Length; i++) {
//                     Concat += errors[i] + "\n";
//                 }
//             }
//           
//             this.f_WarningError(Concat);
//         }
//
//         public void ShowSubmitError(string error = null) {
//             this.f_WarningError(error);
//         }
//
//         public void ShowPage(Action onTransitionComplete = null) {
//             CharacterOnboard_Selection.m_Instance.m_NamingPanel.SetActive(true);
//             onTransitionComplete?.Invoke();
//         }
//
//         public void HidePage(Action onTransitionComplete = null) {
//             CharacterOnboard_Selection.m_Instance.m_NamingPanel.SetActive(false);
//             onTransitionComplete?.Invoke();
//         }
//
//         public void SetNextButtonInteractable(bool isInteractable) {
//             throw new NotImplementedException();
//         }
//     }
// }
