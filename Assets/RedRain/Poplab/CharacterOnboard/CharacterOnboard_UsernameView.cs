using System;
using RedRain.Poplab.View.Onboarding;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace RedRain.Poplab.CharacterOnboard
{

    public class CharacterOnboard_UsernameView : MonoBehaviour, ICharacterNameView
    {
        [SerializeField] TextMeshProUGUI m_submitErrorText;
        [SerializeField] TextMeshProUGUI m_regexErrorText;
        [SerializeField] TMP_InputField m_InputField;
        [SerializeField] Button m_btnBack;
        [SerializeField] Button m_btnNext;
        [SerializeField] GameObject m_UsernameWarning;
        [SerializeField] CanvasGroup m_inputCanvasGrp;
        [SerializeField] CanvasGroup m_inputErrorCanvasGrp;


        public event Action<string> onInputChanged;
        public event Action onClickBack;
        public event Action onClickNext;


        private void Start()
        {
            m_InputField.onValueChanged.AddListener(inputStr => onInputChanged?.Invoke(inputStr));

            m_InputField.onSelect.AddListener(inputStr =>
                                              {
                                                  this.m_inputCanvasGrp.alpha = 1;
                                                  this.m_inputErrorCanvasGrp.alpha = 0;
                                              });

            m_InputField.onDeselect.AddListener(inputStr =>
                                                {
                                                    var _hasError = !string.IsNullOrEmpty(this.m_submitErrorText.text);
                                                    this.m_inputCanvasGrp.alpha = _hasError ? 0 : 1;
                                                    this.m_inputErrorCanvasGrp.alpha = _hasError ? 1 : 0;
                                                });

            m_btnBack.onClick.AddListener(delegate { onClickBack?.Invoke(); });
            m_btnNext.onClick.AddListener(delegate { onClickNext?.Invoke(); });
        }


        public void ShowPage(Action onTransitionComplete = null)
        {
            if(this && this.gameObject)
                this.gameObject.SetActive(true);

            onTransitionComplete?.Invoke();
        }


        public void HidePage(Action onTransitionComplete = null)
        {
            if(this && this.gameObject)
                this.gameObject.SetActive(false);

            onTransitionComplete?.Invoke();
        }


        public void ShowRegexErrors(string[] errors = null)
        {
            // reset text
            this.m_regexErrorText.text = "";

            var _hasErrors = errors != null;
            this.m_UsernameWarning.SetActive(_hasErrors);
            if(!_hasErrors) return;

            // add each line of error
            foreach(var _e in errors)
            {
                if(string.IsNullOrEmpty(_e)) continue;

                this.m_regexErrorText.text += _e + "\n";
            }
        }


        public void ShowSubmitError(string error = null)
        {
            var _hasError = !string.IsNullOrEmpty(error);
            this.m_submitErrorText.text = error;
            this.m_inputCanvasGrp.alpha = _hasError ? 0 : 1;
            this.m_inputErrorCanvasGrp.alpha = _hasError ? 1 : 0;
        }


        public void SetNextButtonInteractable(bool isInteractable)
        {
            m_btnNext.interactable = isInteractable;
        }
    }


}