﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;
namespace RedRain.Poplab.DailyReward
{
    public class DailyRewardGameobject : MonoBehaviour
    {
        public Button m_RewardBtn;
        [SerializeField] private TMP_Text m_Title;
        [SerializeField] private TMP_Text m_RewardName;
        [SerializeField] private Image m_RewardImage;

        public void f_Init(string title, string imageUrl, string rewardName) {
            this.m_Title.text = title;
            this.m_RewardName.text = rewardName;
            StartCoroutine(ie_LoadImageFromWeb(imageUrl));
        }

        IEnumerator ie_LoadImageFromWeb(string imageurl)
        {
            UnityWebRequest www = new UnityWebRequest(imageurl);

            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture2D t_Texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                m_RewardImage.sprite = Sprite.Create(t_Texture, new Rect(0, 0, t_Texture.width, t_Texture.height), Vector2.zero);
            }
        }
    }
}