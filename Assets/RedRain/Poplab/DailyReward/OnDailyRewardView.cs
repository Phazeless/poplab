﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RedRain.Poplab.View.Onboarding;
using System;
using UnityEngine.UI;
using TMPro;

namespace RedRain.Poplab.DailyReward
{
    public class OnDailyRewardView : MonoBehaviour,IDailyRewardsView
    {
        [SerializeField] private List<DailyRewardGameobject> m_DailyRewardsBtn = new List<DailyRewardGameobject>();
        [SerializeField] private Button m_CloseBtn;
        [SerializeField] private TMP_Text m_CountdownTime;
        [SerializeField] private TMP_Text m_CountdownDayTime;
        public event Action onClickGoToButton;
        public event Action<int> onClickUnclaimedReward;
        public event Action onClickClose;

        public void HidePage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(false);
        }

        public void SetRewardAtIndex(int index, string title, string imageUrl, string rewardName)
        {
            m_DailyRewardsBtn[index].f_Init(title, imageUrl, rewardName);
        }

        public void ShowPage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(true);
        }

        public void UpdateCountdownToNextItem(TimeSpan time)
        {
            m_CountdownDayTime.text = time.Hours.ToString() + ":" + time.Minutes.ToString() + ":" + time.Seconds.ToString();
        }

        public void UpdateDaysRemaining(int days)
        {
            m_CountdownDayTime.text = "< color =yellow>" + days.ToString()  +"</color>Days Remaining";
        }

        void Start()
        {
            for (int i = 0; i < m_DailyRewardsBtn.Count; i++) {
                m_DailyRewardsBtn[i].m_RewardBtn.onClick.AddListener(delegate { onClickUnclaimedReward?.Invoke(i); });
            }
            m_CloseBtn.onClick.AddListener(delegate { onClickClose?.Invoke(); });
        }

    }
}