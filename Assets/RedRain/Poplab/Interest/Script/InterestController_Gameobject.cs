﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace RedRain.Poplab.Interest {
    public class InterestController_Gameobject : MonoBehaviour
    {
        Vector3 t_Began;
        Vector3 t_End;
        Touch t_Touch;
        public void Update() {
            if (Input.touchCount > 0 && Interest_Manager.m_Instance.m_CardLeft>0) {
                t_Touch = Input.GetTouch(0);

                switch (t_Touch.phase) {
                    case TouchPhase.Began:
                        OnDown();
                        break;
                    case TouchPhase.Ended:
                        OnUp();
                        break;
                }
            }
        }

        public void OnDown() {
            t_Began = t_Touch.position;
        }

        public void OnUp() {
            t_End = t_Touch.position;
            if (Mathf.Abs(t_Began.x - t_End.x) > Screen.width / 4)
            {
                if (t_End.x > t_Began.x)
                {
                    Interest_Manager.m_Instance.AnimateSwipeLeft();
                    Debug.Log("Right");
                }
                else {
                    Interest_Manager.m_Instance.AnimateSwipeRight();
                }
            }
        }
    }
}
