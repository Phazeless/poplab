﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedRain.Poplab.Interest
{
    public class InterestData_Base
    {
        public int m_InterestIndex;
        public string m_Title;
        public string m_ImageURL;

        public InterestData_Base(int p_Index, string p_Title, string p_ImageUrl)
        {
            m_InterestIndex = p_Index;
            m_Title = p_Title;
            m_ImageURL = p_ImageUrl;
        }
    }
}