﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using MEC;
using TMPro;
using UnityEngine.Networking;

namespace RedRain.Poplab.Interest
{
    public class InterestImage_Gameobject : MonoBehaviour
    {
        public Image m_ContentImage;
        public InterestData_Base m_InterestData;

        public Image m_InterestImage;
        public TextMeshProUGUI m_InterestTitle;

        #region Private Data

        private float t_DistanceMoved;
        private float t_Time = 0;
        private float t_ResetPosTime = 0;

        private bool t_SwipeLeft = false;
        private bool t_ResetPos = false;

        private Vector3 t_IntialPos;
        private Vector3 t_OnEndPos;
        private Vector3 t_Angles;
        private Vector3 t_SmoothVector;
        private Vector3 t_ResetAngle;
        private Vector3 t_OnEndAngles;
        private Vector3 t_ResetVector;

        private Vector2 t_Vector;

        private Color t_DefaultColor = new Color(1, 1, 1, 0);
        private Color t_Color = new Color(1, 1, 1, 1);

        #endregion
        public void f_Init(int index, string title, string imageUrl) {
            m_InterestData = new InterestData_Base(index, title,imageUrl);
            m_InterestTitle.text = m_InterestData.m_Title;
            StartCoroutine(ie_LoadImageFromWeb());
        }

        //public void OnDrag(PointerEventData eventData)
        //{
        //    if (!t_ResetPos)
        //    {
        //        t_Vector = transform.localPosition;
        //        t_Vector.x += eventData.delta.x;

        //        transform.localPosition = t_Vector;

        //        if (transform.localPosition.x - t_IntialPos.x > 0)
        //        {
        //            t_Angles = Vector3.zero;
        //            t_Angles.z = Mathf.LerpAngle(0f, -30f, (t_IntialPos.x + transform.localPosition.x) / (Screen.width / 2));
        //            transform.localEulerAngles = t_Angles;
        //        }
        //        else
        //        {
        //            t_Angles = Vector3.zero;
        //            t_Angles.z = Mathf.LerpAngle(0f, 30f, (t_IntialPos.x - transform.localPosition.x) / (Screen.width / 2));
        //            transform.localEulerAngles = t_Angles;
        //        }
        //    }
        //}

        //public void OnBeginDrag(PointerEventData eventData)
        //{
        //    if (!t_ResetPos)
        //    {
        //        t_IntialPos = transform.localPosition;
        //    }
        //}

        //public void OnEndDrag(PointerEventData eventData)
        //{
        //    if (!t_ResetPos)
        //    {
        //        t_DistanceMoved = Mathf.Abs(transform.localPosition.x - t_IntialPos.x);
        //        if (t_DistanceMoved < .4f * Screen.width)
        //        {
        //            //transform.localPosition = t_IntialPos;
        //            //transform.localEulerAngles = Vector3.zero;
        //            t_ResetVector = transform.localPosition;
        //            t_OnEndPos = transform.localPosition;

        //            t_ResetAngle = transform.localEulerAngles;
        //            t_OnEndAngles = transform.localEulerAngles;

        //            t_ResetPos = true;
        //            t_ResetPosTime = 0;
        //        }
        //        else
        //        {
        //            if (transform.localPosition.x > t_IntialPos.x)
        //            {
        //                //SwipeRight
        //                t_SwipeLeft = false;
        //            }
        //            else
        //            {
        //                //SwipeLeft
        //                t_SwipeLeft = true;
        //            }

        //            Timing.RunCoroutine(ie_MoveCard());
        //        }
        //    }
        //}

        //public void Update() {
        //    if (t_ResetPos) {
        //        t_ResetPosTime += Time.deltaTime;
        //        if (transform.localPosition.x != t_IntialPos.x) {
        //            t_ResetVector.x = Mathf.SmoothStep(t_OnEndPos.x,t_IntialPos.x,3f*t_ResetPosTime);
        //            transform.localPosition = t_ResetVector;
        //        }

        //        if(transform.localEulerAngles.z != 0) {
        //            t_ResetAngle.z = Mathf.LerpAngle(t_OnEndAngles.z, 0,3f* t_ResetPosTime);
        //            transform.localEulerAngles = t_ResetAngle;
        //        }

        //        if (transform.localEulerAngles.x == 0 && transform.localPosition.x == 0) {
        //            t_ResetPos = false;
        //        }
        //    }
        //}

        public void f_AnimateImage(bool p_Left) {
            Timing.RunCoroutine(ie_MoveCard(p_Left));
        }

        IEnumerator<float> ie_MoveCard(bool p_Left) {
            t_Time = 0f;
            t_SmoothVector = Vector3.zero;

            while (m_ContentImage.color != t_DefaultColor) {
                t_Time += Time.deltaTime;
                if (p_Left)
                {
                    t_SmoothVector.x = Mathf.SmoothStep(transform.localPosition.x, transform.localPosition.x - Screen.width,t_Time);
                    t_SmoothVector.y = transform.localPosition.y;
                }
                else {
                    t_SmoothVector.x = Mathf.SmoothStep(transform.localPosition.x, transform.localPosition.x + Screen.width,t_Time);
                    t_SmoothVector.y = transform.localPosition.y;
                }
                transform.localPosition = t_SmoothVector;
                t_Color.a = Mathf.SmoothStep(1f, 0f, 3* t_Time);
                m_ContentImage.color = t_Color;
                yield return Timing.WaitForOneFrame;
            }

            Interest_Manager.m_Instance.f_DecreaseCard();

            gameObject.SetActive(false);
            yield return Timing.WaitForOneFrame;
        }

        IEnumerator ie_LoadImageFromWeb()
        {
            UnityWebRequest www = new UnityWebRequest(m_InterestData.m_ImageURL);
            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture2D t_Texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                m_InterestImage.sprite = Sprite.Create(t_Texture, new Rect(0, 0, t_Texture.width, t_Texture.height), Vector2.zero);
            }
        }
    }
}