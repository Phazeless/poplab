﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RedRain.Poplab.View.Onboarding;
using System;

namespace RedRain.Poplab.Interest
{
    public class Interest_Manager : MonoBehaviour,IOpeningInterestsView
    {
        public static Interest_Manager m_Instance;

        public GameObject m_FinishPanel;
        public Transform m_Parent;
        public InterestImage_Gameobject m_Prefab;
        public int m_CardLeft;

        private List<InterestImage_Gameobject> m_InterestedList = new List<InterestImage_Gameobject>();
        InterestImage_Gameobject t_Obj;

        public event Action<int, bool> onInterestEvent;

        public void Awake()
        {
            m_Instance = this;
        }
        void Start()
        {
            m_CardLeft = 0;
        }

        public void f_DecreaseCard( ) {
            m_CardLeft--;
            if (m_CardLeft <= 0) {
                m_FinishPanel.SetActive(true);
            }
        }

        public void AddInterestToStack(int index, string title, string imageUrl)
        {
            t_Obj = Instantiate(m_Prefab);
            t_Obj.transform.SetParent(m_Parent);
            t_Obj.f_Init(index, title, imageUrl);
            m_InterestedList.Add(t_Obj);
            m_CardLeft = m_InterestedList.Count - 1;
        }

        public void AnimateSwipeLeft()
        {
            m_InterestedList[m_CardLeft].f_AnimateImage(true);
            f_DecreaseCard();

        }

        public void AnimateSwipeRight()
        {
            m_InterestedList[m_CardLeft].f_AnimateImage(false);
            f_DecreaseCard();
        }

        public void ShowPage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(true);
        }

        public void HidePage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(false);
        }


    }
}