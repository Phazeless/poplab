using UnityEngine;


namespace RedRain.Poplab.OpeningOnboard {
    public class OpeningOnboard_Indicator : MonoBehaviour {
        public GameObject m_IndicatorOn;
        public GameObject m_IndicatorOff;

        public void f_TurnOn() {
            this.m_IndicatorOn.SetActive(true);
            this.m_IndicatorOff.SetActive(false);
        }

        public void f_TurnOff() {
            this.m_IndicatorOn.SetActive(false);
            this.m_IndicatorOff.SetActive(true);
        }
    }
}
