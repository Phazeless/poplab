using System;
using System.Collections.Generic;
using RedRain.Poplab.View.Onboarding;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace RedRain.Poplab.OpeningOnboard {
    public class OpeningOnboard_Manager : MonoBehaviour, IOpeningOnboardView {
        [System.Serializable]
        public class c_InterfaceComponent {
            public TextMeshProUGUI m_Title;
            public TextMeshProUGUI m_Description;
            public Image m_Image;
        }public c_InterfaceComponent m_Component = new c_InterfaceComponent();

        [System.Serializable]
        public class c_Information {
            public string m_Title;
            [TextArea] public string m_Description;
            public Sprite m_Image;
        }public List<c_Information> m_Information = new List<c_Information>();

        public GameObject m_NextButton;
        public GameObject m_IndicatorContainer;
        public GameObject m_IndicatorPrefab;

        int m_CurrentPage;

        public event Action onNext;
        public event Action onPrevious;

        // Start is called before the first frame update
        void Start() {
            this.m_NextButton.SetActive(false);
            this.m_CurrentPage = 0;
            this.f_SpawnIndicator();
        }

        // Update is called once per frame
        void Update() {
            if (this.m_CurrentPage == this.m_Information.Count - 1) this.m_NextButton.SetActive(true);
            else this.m_NextButton.SetActive(false);
        }

        public void f_SpawnIndicator() {
            for(int i=0;i< this.m_Information.Count; i++) {
                if(i == 0)Instantiate(this.m_IndicatorPrefab, this.m_IndicatorContainer.transform).GetComponent<OpeningOnboard_Indicator>().f_TurnOn();
                else Instantiate(this.m_IndicatorPrefab, this.m_IndicatorContainer.transform).GetComponent<OpeningOnboard_Indicator>().f_TurnOff();
            }
        }

        public void f_PageNavigation(int t_Page) {
            this.m_CurrentPage += t_Page;
            if (t_Page > 0) this.onNext?.Invoke();
            else this.onPrevious?.Invoke();
            this.f_ShowPageNavigation();
        }

        public void f_ShowPageNavigation() {
            if (this.m_CurrentPage > this.m_Information.Count - 1) this.m_CurrentPage = this.m_Information.Count - 1;
            else if (this.m_CurrentPage < 0) this.m_CurrentPage = 0;
            //Shifting Indicator
            for (int i = 0; i < this.m_Information.Count; i++) this.m_IndicatorContainer.transform.GetChild(i).GetComponent<OpeningOnboard_Indicator>().f_TurnOff();
            this.m_IndicatorContainer.transform.GetChild(this.m_CurrentPage).GetComponent<OpeningOnboard_Indicator>().f_TurnOn();
            //Parsing Information
            this.m_Component.m_Title.text = this.m_Information[this.m_CurrentPage].m_Title;
            this.m_Component.m_Description.text = this.m_Information[this.m_CurrentPage].m_Description;
            this.m_Component.m_Image.sprite = this.m_Information[this.m_CurrentPage].m_Image;
        }

        public void ShowIndex(int index) {
            this.m_CurrentPage = index;
            this.f_ShowPageNavigation();
        }

        public void ShowPage(Action onTransitionComplete = null) {
            this.gameObject.SetActive(true);
            onTransitionComplete?.Invoke();
        }

        public void HidePage(Action onTransitionComplete = null) {
            this.gameObject.SetActive(false);
            onTransitionComplete?.Invoke();
        }
    }
}

