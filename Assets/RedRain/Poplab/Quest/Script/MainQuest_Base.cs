﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedRain.Poplab.Quest
{
    public class MainQuest_Base
    {
        public long m_QuestID;
        public string m_DisplayName;
        public string m_DisplayExpireInfo;
        public string m_ImageUrl;

        public MainQuest_Base(long p_QuestID,string p_DisplayName,string p_DisplayExpireInfo,string p_ImageUrl) {
            m_QuestID = p_QuestID;
            m_DisplayName = p_DisplayName;
            m_DisplayExpireInfo = p_DisplayExpireInfo;
            m_ImageUrl = p_ImageUrl;
        }
    }
}
