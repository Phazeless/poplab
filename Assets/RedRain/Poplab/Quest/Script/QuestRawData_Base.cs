﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedRain.Poplab.Quest
{   
    [Serializable]
    public class QuestRawData_Base
    {
        [Serializable]
        public class c_SubQuestRawData {
            public string m_SubQuestCode;
            public string m_SubQuestDesc;
            public int m_Target;
            public int m_CurrentProgress;
            public int m_Reward;
            public bool m_Claimed;
        }

        public string m_MainQuestCode;
        public List<c_SubQuestRawData> m_SubQuesRawDatas;
        public string m_MainQuestDesc;
        public int m_Year;
        public int m_Month;
        public int m_Date;
        public Sprite m_QuestIcon;
        public Sprite m_QuestRewardIcon;
        public int m_MainQuestReward;
    }
}