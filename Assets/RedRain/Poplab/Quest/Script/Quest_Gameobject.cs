﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using RedRain.Poplab.View.Quest;
using UnityEngine.Networking;
namespace RedRain.Poplab.Quest
{
    public class Quest_Gameobject : MonoBehaviour , IQuestRowView
    {
        [SerializeField] private Button m_ArrowDownButton;
        [SerializeField] private Image m_MainQuestIcon;
        [SerializeField] private TextMeshProUGUI m_QuestDisplayName;
        [SerializeField] private TextMeshProUGUI m_ExpireDateText;
        public Transform m_SubQuestContent;

        private MainQuest_Base m_MainQuestData;

        public event Action<long> onClickArrow;

        public void f_Init() {
            m_QuestDisplayName.text = m_MainQuestData.m_DisplayName;
            
            m_ExpireDateText.text = m_MainQuestData.m_DisplayExpireInfo;
            m_ArrowDownButton.onClick.AddListener(delegate { onClickArrow?.Invoke(m_MainQuestData.m_QuestID); });
        }

        public void SetInfo(long questId, string displayName, string displayExpiryInfo, string imageUrl)
        {
            m_MainQuestData = new MainQuest_Base(questId,displayName,displayExpiryInfo,imageUrl);
            f_Init();
            StartCoroutine(ie_LoadImageFromWeb());
        }

        public void UpdateArrowDirection(bool isUp)
        {
            m_SubQuestContent.gameObject.SetActive(!isUp);
        }

        IEnumerator ie_LoadImageFromWeb() {
            UnityWebRequest www = new UnityWebRequest(m_MainQuestData.m_ImageUrl);

            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else {
                Texture2D t_Texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                m_MainQuestIcon.sprite = Sprite.Create(t_Texture,new Rect(0,0,t_Texture.width,t_Texture.height),Vector2.zero);
            }
        }
    }
}