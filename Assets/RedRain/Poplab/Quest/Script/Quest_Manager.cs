﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RedRain.Poplab.View.Quest;
using System;
using UnityEngine.UI;

namespace RedRain.Poplab.Quest {
    public class Quest_Manager : MonoBehaviour,IQuestPageView
    {
        [SerializeField] private Transform m_Content;

        [SerializeField] private Quest_Gameobject m_MainQuestPrefab;
        [SerializeField] private SubQuest_Gameobject m_SubQuestPrefab;
        [SerializeField] private Reward_Gameobject m_RewardPrefab;
        [SerializeField] private Button m_DailyRewardButton;
        [SerializeField] private Button m_BackButton;

        public event Action onClickDailyRewards;
        public event Action onClickBack;

        private Quest_Gameobject m_LastMainQuestGenerated;

        private void Start()
        {
            m_DailyRewardButton.onClick.AddListener(delegate { onClickDailyRewards?.Invoke(); });
            m_BackButton.onClick.AddListener(delegate { onClickBack?.Invoke(); });
        }

        public IQuestRowView CreateNewQuestRow()
        {
            m_LastMainQuestGenerated = Instantiate(m_MainQuestPrefab);
            m_LastMainQuestGenerated.transform.SetParent(m_Content);
            return m_LastMainQuestGenerated;
        }

        public IRewardRowView CreateNewRewardRow()
        {
            Reward_Gameobject t_Reward = Instantiate(m_RewardPrefab);
            t_Reward.transform.SetParent(m_LastMainQuestGenerated.m_SubQuestContent);
            return t_Reward;
        }

        public ITaskRowView CreateNewTaskRow()
        {
            SubQuest_Gameobject t_SubQuest = Instantiate(m_SubQuestPrefab);
            t_SubQuest.transform.SetParent(m_LastMainQuestGenerated.m_SubQuestContent);
            return t_SubQuest;
        }

        public void HidePage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(false);
        }

        public void ShowPage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(true);
        }
    }
}