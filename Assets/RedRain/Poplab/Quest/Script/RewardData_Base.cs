﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedRain.Poplab.Quest
{
    [System.Serializable]
    public class RewardData_Base
    {
        public string m_ImageURL;
        public int m_RewardQty;

        public RewardData_Base(string p_ImageURl, int p_rewardQty)
        {
            m_ImageURL = p_ImageURl;
            m_RewardQty = p_rewardQty;
        }
    }
}