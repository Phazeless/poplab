﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RedRain.Poplab.View.Quest;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;

namespace RedRain.Poplab.Quest {
    public class Reward_Gameobject : MonoBehaviour, IRewardRowView
    {
        private RewardData_Base m_RewardData;

        [SerializeField] private Image m_RewardImg;
        [SerializeField] private Image m_RewardBigImage;
        [SerializeField] private TextMeshProUGUI m_RewardQtyText;

        public bool m_Claimed = false;

        public void f_Init() {
            if (m_RewardData.m_RewardQty > 1)
            {
                m_RewardQtyText.text = m_RewardData.m_RewardQty.ToString();
            }
            else {
                m_RewardQtyText.text = "";
            }
            StartCoroutine(ie_LoadImageFromWeb());
        }

        public void AddRewardItem(string imageUrl, int rewardQty = 1)
        {
            m_RewardData = new RewardData_Base(imageUrl,rewardQty);
            f_Init();
        }

        public void SetRewardState(bool isClaimed)
        {
            m_Claimed = isClaimed;
        }

        IEnumerator ie_LoadImageFromWeb()
        {
            UnityWebRequest www = new UnityWebRequest(m_RewardData.m_ImageURL);
            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture2D t_Texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                m_RewardImg.sprite = Sprite.Create(t_Texture, new Rect(0, 0, t_Texture.width, t_Texture.height), Vector2.zero);
                m_RewardBigImage.sprite = m_RewardImg.sprite;
            }
        }
    }

}