﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedRain.Poplab.Quest
{
    public class SubQuest_Base
    {
        public long m_TaskId;
        public string m_SubQuestDisplayName;
        public string m_RewardImageUrl;

        public int m_Target;
        public int m_CurrentProgress;
        public int m_RewardQuantity;

        public bool m_Claimable;
        public SubQuest_Base(long p_TaskId,string p_DisplayName,int p_CurrProgress,int p_TotalProgress,string p_RewardImageUrl,int p_rewardQty,bool p_isGoBtnInteractabl){
            m_TaskId = p_TaskId;
            m_SubQuestDisplayName = p_DisplayName;
            m_RewardImageUrl = p_RewardImageUrl;
            m_Target = p_TotalProgress;
            m_CurrentProgress = p_CurrProgress;
            m_RewardQuantity = p_rewardQty;
            m_Claimable = p_isGoBtnInteractabl;
        }
    }
}
