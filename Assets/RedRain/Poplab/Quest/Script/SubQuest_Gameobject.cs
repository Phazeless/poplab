﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using RedRain.Poplab.View.Quest;
using System;
using UnityEngine.Networking;

namespace RedRain.Poplab.Quest
{
    public class SubQuest_Gameobject : MonoBehaviour,ITaskRowView
    {
        private SubQuest_Base m_SubQuestData;

        [SerializeField] private Image m_Progress;
        [SerializeField] private Image m_RewardImage;
        [SerializeField] private Button m_ClaimButton;

        [SerializeField] private TextMeshProUGUI m_RewardQty;
        [SerializeField] private TextMeshProUGUI m_QuestDesc;
        [SerializeField] private TextMeshProUGUI m_QuestProgress;

        public event Action<long> onClickGo;

        public void f_Init() {
            m_Progress.fillAmount = (float)m_SubQuestData.m_CurrentProgress / (float)m_SubQuestData.m_Target;
            m_ClaimButton.interactable = m_SubQuestData.m_Claimable;
            m_RewardQty.text = m_SubQuestData.m_RewardQuantity.ToString();
            m_QuestDesc.text = m_SubQuestData.m_SubQuestDisplayName;
            m_QuestProgress.text = m_SubQuestData.m_CurrentProgress.ToString() + "/" + m_SubQuestData.m_Target.ToString();
            m_ClaimButton.onClick.AddListener(delegate { onClickGo?.Invoke(m_SubQuestData.m_TaskId); });
            StartCoroutine(ie_LoadImageFromWeb());
        }

        public void SetHeight(float normalisedSize)
        {
            //This will not be used as The task row is controlled by layout group
            //as such there is no way to set the height of the task record
        }

        public void SetInfo(long taskId, string displayName, int currProgress, int totalProgress, string rewardImageUrl, int rewardQty, bool isGoBtnInteractable)
        {
            m_SubQuestData = new SubQuest_Base(taskId, displayName, currProgress, totalProgress, rewardImageUrl, rewardQty, isGoBtnInteractable);
            f_Init();
        }

        IEnumerator ie_LoadImageFromWeb()
        {
            UnityWebRequest www = new UnityWebRequest(m_SubQuestData.m_RewardImageUrl);

            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture2D t_Texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                m_RewardImage.sprite = Sprite.Create(t_Texture, new Rect(0, 0, t_Texture.width, t_Texture.height), Vector2.zero);
            }
        }
    }
}