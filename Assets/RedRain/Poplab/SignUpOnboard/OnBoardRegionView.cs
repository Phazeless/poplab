﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RedRain.Poplab.View.Onboarding;
using System;
using UnityEngine.UI;

namespace RedRain.Poplab.SignUpOnboard
{
    public class OnBoardRegionView : MonoBehaviour, IOpenningSelectRegionView
    {
        [SerializeField] private Button m_BtnNext;
        [SerializeField] private Button[] m_RegionsButton;
        public int activeIndex
        {
            get {
                return t_ActiveIndex;
            }
        }

        private int t_ActiveIndex;
        public event Action<int> onIndexChanged;
        public event Action onNext;

        private void Start()
        {
            for (int i = 0; i < m_RegionsButton.Length; i++) {
                m_RegionsButton[i].onClick.AddListener(delegate { onIndexChanged?.Invoke(i); });
            }
            m_BtnNext.onClick.AddListener(delegate { onNext?.Invoke(); });
        }

        public void HidePage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(false);
        }

        public void SetNextButtonInteractable(bool isInteractable)
        {
            m_BtnNext.interactable = isInteractable;
        }

        public void ShowPage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(true);
        }
    }
}
