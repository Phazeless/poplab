﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RedRain.Poplab.View.Onboarding;
using System;
using TMPro;

namespace RedRain.Poplab.SignUpOnboard
{
    public class OnBoardSignUpView : MonoBehaviour, IOnboardSignupView
    {
        [SerializeField] private TMP_InputField m_TxtFullName;
        [SerializeField] private TMP_InputField m_TxtEmail;
        [SerializeField] private TMP_InputField m_TextPassword;
        [SerializeField] private TMP_InputField m_TextConfirmPassword;
        [SerializeField] private Toggle m_TogglePassword;
        [SerializeField] private Toggle m_ToggleConfirmPassword;
        [SerializeField] private Button m_BtnSignUp;
        [SerializeField] private Button m_BtnSignIn;
        [SerializeField] private Button m_BtnPrivacyPolicy;
        [SerializeField] private Button m_BtnTC;
        // [SerializeField] private GameObject m_SuccesPage;
        [SerializeField] private Button m_btnFacebook;
        [SerializeField] private Button m_btnGoogle;
        [SerializeField] private Button m_btnApple;
        [SerializeField] private Button m_btnWeChat;
        [SerializeField] private TMP_Text m_textError;
        public event Action<string> onFullNameChanged;
        public event Action<string> onEmailChanged;
        public event Action<string> onPasswordChanged;
        public event Action<string> onConfirmPasswordChanged;
        public event Action<bool> onTogglePasswordConceal;
        public event Action<bool> onToggleConfirmPasswordConceal;
        public event Action onClickSignUpButton;
        public event Action onClickSignIn;
        public event Action onClickTermsAndCond;
        public event Action onClickPrivacyPolicy;
        public event Action onClickAppleSignup;
        public event Action onClickFacebookSignup;
        public event Action onClickGoogleSignup;
        public event Action onClickWechatSignup;

        private void Start() {
            this.m_TxtFullName.onValueChanged.AddListener(delegate (string fullname) { this.onFullNameChanged?.Invoke(fullname); });
            this.m_TxtEmail.onValueChanged.AddListener(delegate (string email) { this.onEmailChanged?.Invoke(email); });
            this.m_TextPassword.onValueChanged.AddListener(delegate (string pw) { this.onPasswordChanged?.Invoke(pw); });
            this.m_TextConfirmPassword.onValueChanged.AddListener(delegate (string pw) { this.onConfirmPasswordChanged?.Invoke(pw); });
            this.m_TogglePassword.onValueChanged.AddListener(delegate (bool isConcealed) { this.onTogglePasswordConceal?.Invoke(isConcealed); });
            this.m_ToggleConfirmPassword.onValueChanged.AddListener(delegate (bool isConcealed) { this.onToggleConfirmPasswordConceal?.Invoke(isConcealed); });
            this.m_BtnSignUp.onClick.AddListener(delegate { onClickSignUpButton?.Invoke(); });
            this.m_BtnSignIn.onClick.AddListener(delegate { onClickSignIn?.Invoke(); });
            this.m_BtnPrivacyPolicy.onClick.AddListener(delegate { onClickPrivacyPolicy?.Invoke(); });
            this.m_BtnTC.onClick.AddListener(delegate { onClickTermsAndCond?.Invoke(); });
        }

        public void ConcealConfirmPassword(bool isConcealed)
        {
            // Ting: need to disable and enable for contentType change to take effect
            this.m_TextConfirmPassword.enabled = false;
            this.m_TextConfirmPassword.contentType = isConcealed
                                            ? TMP_InputField.ContentType.Password
                                            : TMP_InputField.ContentType.Standard;
            this.m_TextConfirmPassword.enabled = true;
        }

        public void ConcealPassword(bool isConcealed)
        {
            // Ting: need to disable and enable for contentType change to take effect
            this.m_TextPassword.enabled = false;
            this.m_TextPassword.contentType = isConcealed
                                            ? TMP_InputField.ContentType.Password
                                            : TMP_InputField.ContentType.Standard;
            this.m_TextPassword.enabled = true;
        }

        public void HidePage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(false);
        }

        public void SetSignUpButtonInteractable(bool isInteractable)
        {
            this.m_BtnSignUp.interactable = isInteractable;
        }

        public void ShowPage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(true);
        }

        public void ShowRegexErrors(string[] errors = null)
        {
            this.m_textError.text = "";
            if (errors == null) {
                return;
            }

            for (int i = 0; i < errors.Length; i++) {
                if (!string.IsNullOrEmpty(errors[i]))
                {
                    if(!string.IsNullOrEmpty(this.m_textError.text)) this.m_textError.text += "\n";
                    this.m_textError.text += errors[i];
                }
                else continue;
            }
        }
    }
}