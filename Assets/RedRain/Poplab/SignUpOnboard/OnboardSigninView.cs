using System;
using RedRain.Poplab.View.Onboarding;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace RedRain.Poplab.SignUpOnboard
{

    public class OnboardSigninView : MonoBehaviour, IOnboardSigninView
    {
        [SerializeField] private TMP_InputField m_txtEmail;
        [SerializeField] private TMP_InputField m_txtPassword;
        [SerializeField] private Toggle m_togConceal;
        [SerializeField] private Button m_btnForgotPassword;
        [SerializeField] private Button m_btnLogin;
        [SerializeField] private Button m_btnFacebook;
        [SerializeField] private Button m_btnGoogle;
        [SerializeField] private Button m_btnApple;
        [SerializeField] private Button m_btnSignUp;
        [SerializeField] private GameObject m_objError;
        [SerializeField] private TMP_Text m_txtError;
        [SerializeField] private CanvasGroup m_canvasGroupPassword;

        public event Action<string> onEmailChanged;
        public event Action<string> onPasswordChanged;
        public event Action<bool> onTogglePasswordConceal;
        public event Action onClickLoginButton;
        public event Action onClickForgotPassword;
        public event Action onClickSignup;
        public event Action onClickAppleLogin; // not yet implemented
        public event Action onClickFacebookLogin; // not yet implemented
        public event Action onClickGoogleLogin; // not yet implemented
        public event Action onClickWechatLogin; // not yet implemented


        private void Start()
        {
            this.m_txtPassword.onSelect.AddListener(delegate(string arg0)
                                                    {
                                                        // hide error message when user clicks on the input field
                                                        this.m_objError.SetActive(false);
                                                        this.m_canvasGroupPassword.alpha = 1;
                                                    });

            this.m_txtPassword.onDeselect.AddListener(delegate(string arg0)
                                                      {
                                                          // show password if error txt is not empty 
                                                          var _showPassword = string.IsNullOrEmpty(this.m_txtError.text);

                                                          // show error message if either pw string is empty AND error txt is not empty
                                                          this.m_objError.SetActive(!_showPassword);
                                                          this.m_canvasGroupPassword.alpha = _showPassword ? 1 : 0;
                                                      });

            this.m_txtPassword.onValueChanged.AddListener(delegate(string pw) { this.onPasswordChanged?.Invoke(pw); });
            this.m_txtEmail.onValueChanged.AddListener(delegate(string email) { this.onEmailChanged?.Invoke(email); });
            this.m_togConceal.onValueChanged.AddListener(delegate(bool isConcealed) { this.onTogglePasswordConceal?.Invoke(isConcealed); });
            this.m_btnForgotPassword.onClick.AddListener(delegate { this.onClickForgotPassword?.Invoke(); });
            this.m_btnLogin.onClick.AddListener(delegate { this.onClickLoginButton?.Invoke(); });
            this.m_btnSignUp.onClick.AddListener(delegate { this.onClickSignup?.Invoke(); });
            ShowError(null);

            this.m_btnFacebook.onClick.AddListener(delegate { this.onClickFacebookLogin?.Invoke(); });
            this.m_btnGoogle.onClick.AddListener(delegate { this.onClickGoogleLogin?.Invoke(); });
            this.m_btnApple.onClick.AddListener(delegate { this.onClickAppleLogin?.Invoke(); });
        }


        public void ConcealPassword(bool isConcealed)
        {
            this.m_txtPassword.enabled = false;

            this.m_txtPassword.contentType = isConcealed
                                                 ? TMP_InputField.ContentType.Password
                                                 : TMP_InputField.ContentType.Standard;

            this.m_txtPassword.enabled = true;
        }


        public void ShowError(string message)
        {
            if(!string.IsNullOrEmpty(message))
            {
                this.m_canvasGroupPassword.alpha = 0;
                this.m_objError.SetActive(true);
                this.m_txtError.text = message;
                return;
            }

            this.m_canvasGroupPassword.alpha = 1;
            this.m_objError.SetActive(false);
            this.m_txtError.text = "";
        }


        public void SetSignInButtonInteractable(bool isInteractable)
        {
            this.m_btnLogin.interactable = isInteractable;
        }


        public void ShowPage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(true);
        }


        public void HidePage(Action onTransitionComplete = null)
        {
            this.gameObject.SetActive(false);
        }
    }

}