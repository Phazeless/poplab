﻿using System;
using RedRain.Poplab.View.Onboarding;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RedRain.Poplab.Verification {

    public class ForgotPasswordView : MonoBehaviour, IPopupForgotPasswordView {
        [SerializeField] private TMP_InputField m_EmailInput;
        [SerializeField] private TextMeshProUGUI m_ErrorText;
        [SerializeField] private Button m_ContinueButton;
        [SerializeField] private Button m_BackButton;

        public event Action<string> onEmailChanged;
        public event Action onClickContinue;
        public event Action onClickBack;

        void Start() {
            this.m_ContinueButton.onClick.AddListener(() => this.onClickContinue?.Invoke());
            this.m_BackButton.onClick.AddListener(() => this.onClickBack?.Invoke());
            this.m_EmailInput.onValueChanged.AddListener(inputStr => this.onEmailChanged?.Invoke(inputStr));
        }

        public void HidePage(Action onTransitionComplete = null) {
            this.gameObject.SetActive(false);
            onTransitionComplete?.Invoke();
        }

        public void SetContinueButtonInteractable(bool isInteractable) {
            this.m_ContinueButton.interactable = isInteractable;
        }

        public void ShowError(string message) {
            if (!string.IsNullOrEmpty(message)) {
                this.m_ErrorText.gameObject.SetActive(true);
                this.m_ErrorText.text = message;
                return;
            }

            this.m_ErrorText.gameObject.SetActive(false);
        }

        public void ShowPage(Action onTransitionComplete = null) {
            this.gameObject.SetActive(true);
            onTransitionComplete?.Invoke();
        }
    }
}