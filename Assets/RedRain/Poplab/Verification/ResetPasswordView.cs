﻿using System;
using RedRain.Poplab.View.Onboarding;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace RedRain.Poplab.Verification
{

    public class ResetPasswordView : MonoBehaviour, IPopupResetPasswordView
    {
        [SerializeField] private TMP_InputField m_PasswordInput;
        [SerializeField] private TMP_InputField m_ConfirmPasswordInput;
        [SerializeField] private TextMeshProUGUI m_ErrorPasswordText;
        [SerializeField] private TextMeshProUGUI m_ErrorConfirmPasswordText;
        [SerializeField] private Button m_ResetButton;
        [SerializeField] private Button m_BackButton;
        [SerializeField] private Toggle m_ToogleConcealConfirmPassword;
        [SerializeField] private Toggle m_ToogleConcealPassword;

        public event Action<string> onPasswordChanged;
        public event Action<string> onConfirmPasswordChanged;
        public event Action<bool> onTogglePasswordConceal;
        public event Action<bool> onToggleConfirmPasswordConceal;
        public event Action onClickReset;
        public event Action onClickBack;

        void Start() {
            this.m_ResetButton.onClick.AddListener(() => this.onClickReset?.Invoke());
            this.m_BackButton.onClick.AddListener(() => this.onClickBack?.Invoke());
            this.m_PasswordInput.onValueChanged.AddListener(inputStr => this.onPasswordChanged?.Invoke(inputStr));
            this.m_ConfirmPasswordInput.onValueChanged.AddListener(inputStr => this.onConfirmPasswordChanged?.Invoke(inputStr));
            this.m_ToogleConcealPassword.onValueChanged.AddListener(isConcealed => this.onTogglePasswordConceal?.Invoke(isConcealed));
            this.m_ToogleConcealConfirmPassword.onValueChanged.AddListener(isConcealed => this.onToggleConfirmPasswordConceal?.Invoke(isConcealed));
        }

        public void ConcealConfirmPassword(bool isConcealed) {
            this.m_ConfirmPasswordInput.contentType = isConcealed
                                                 ? TMP_InputField.ContentType.Password
                                                 : TMP_InputField.ContentType.Standard;
        }

        public void ConcealPassword(bool isConcealed) {
            this.m_PasswordInput.contentType = isConcealed
                                               ? TMP_InputField.ContentType.Password
                                               : TMP_InputField.ContentType.Standard;
        }

        public void HidePage(Action onTransitionComplete = null) {
            this.gameObject.SetActive(false);
            onTransitionComplete?.Invoke();
        }

        public void SetResetPasswordButtonInteractable(bool isInteractable) {
            this.m_ResetButton.interactable = isInteractable;
        }

        public void ShowError(string message) {
            //this one must be added IF, there two types of errors, the New Password one, and the Confirm Password one.
            //this.m_ErrorConfirmPasswordText.gameObject.SetActive(true);
            if (!string.IsNullOrEmpty(message)) {
                this.m_ErrorPasswordText.gameObject.SetActive(true);
                this.m_ErrorPasswordText.text = message;
                return;
            }

            this.m_ErrorPasswordText.gameObject.SetActive(false); 
        }

        public void ShowPage(Action onTransitionComplete = null) {
            this.gameObject.SetActive(true);
            onTransitionComplete?.Invoke();
        }

    }

}
