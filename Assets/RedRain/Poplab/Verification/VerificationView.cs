﻿using System;
using RedRain.Poplab.View.Onboarding;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RedRain.Poplab.Verification {
    public class VerificationView : MonoBehaviour, IPopupEnterVerificationCodeView {
        [SerializeField] private TMP_InputField[] m_InputChar;
        [SerializeField] private GameObject m_ResendButtonObject;
        [SerializeField] private TextMeshProUGUI m_InvalidCodeText;
        [SerializeField] private TextMeshProUGUI m_HasBeenSentText;
        [SerializeField] private Button m_ResendButton;
        [SerializeField] private Button m_ContinueButton;
        [SerializeField] private Button m_BackButton;

        public event Action<int> onChar1Changed;
        public event Action<int> onChar2Changed;
        public event Action<int> onChar3Changed;
        public event Action<int> onChar4Changed;
        public event Action<int> onChar5Changed;
        public event Action<int> onChar6Changed;
        public event Action onClickResendCode;
        public event Action onClickContinue;
        public event Action onClickBack;

        void Start() {
            m_InputChar[0].onValueChanged.AddListener(inputStr => this.onChar1Changed(Convert.ToInt32(inputStr)));
            m_InputChar[1].onValueChanged.AddListener(inputStr => this.onChar2Changed(Convert.ToInt32(inputStr)));
            m_InputChar[2].onValueChanged.AddListener(inputStr => this.onChar3Changed(Convert.ToInt32(inputStr)));
            m_InputChar[3].onValueChanged.AddListener(inputStr => this.onChar4Changed(Convert.ToInt32(inputStr)));
            m_InputChar[4].onValueChanged.AddListener(inputStr => this.onChar5Changed(Convert.ToInt32(inputStr)));
            m_InputChar[5].onValueChanged.AddListener(inputStr => this.onChar6Changed(Convert.ToInt32(inputStr)));
            this.m_ResendButton.onClick.AddListener(() => this.onClickResendCode?.Invoke());
            this.m_BackButton.onClick.AddListener(() => this.onClickBack?.Invoke());
            this.m_ContinueButton.onClick.AddListener(() => this.onClickContinue?.Invoke());
        }

        public void HidePage(Action onTransitionComplete = null) {
            this.gameObject.SetActive(false);
            onTransitionComplete?.Invoke();
        }

        public void SetContinueButtonInteractable(bool isInteractable) {
            this.m_ContinueButton.interactable = isInteractable;
        }

        public void ShowHideResendCodeButton(bool show) {
            this.m_ResendButtonObject.SetActive(show);
        }

        public void ShowLabel(string message, bool isError = false) {
            this.m_InvalidCodeText.gameObject.SetActive(false);
            this.m_HasBeenSentText.gameObject.SetActive(false);

            if (isError) {
                this.m_InvalidCodeText.gameObject.SetActive(true);
                this.m_InvalidCodeText.text = message;
                return;
            }

            this.m_HasBeenSentText.gameObject.SetActive(true);
            this.m_HasBeenSentText.text = message;
        }

        public void ShowPage(Action onTransitionComplete = null) {
            this.gameObject.SetActive(true);
            onTransitionComplete?.Invoke();
        }

    }
}
