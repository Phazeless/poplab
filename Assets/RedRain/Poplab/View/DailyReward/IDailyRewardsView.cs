using System;


namespace RedRain.Poplab.View.Onboarding
{

    /// <summary>
    /// View class for BOTH Daily Rewards and Login Rewards page.
    /// </summary>
    public interface IDailyRewardsView : IPageView
    {


        #region Input Events

        /// <summary>
        /// Invoked when user clicks the go to Login Rewards / Daily Login button (subscribed by Poplab Controller).
        /// </summary>
        event Action onClickGoToButton;

        /// <summary>
        /// Invoked when user clicks on an unclaimed reward (subscribed by Poplab Controller).
        /// </summary>
        event Action<int> onClickUnclaimedReward;

        /// <summary>
        /// Invoked when user clicks the Close button (subscribed by Poplab Controller).
        /// </summary>
        event Action onClickClose;

        #endregion


        void UpdateDaysRemaining(int days);
        void UpdateCountdownToNextItem(TimeSpan time);
        void SetRewardAtIndex(int index, string title, string imageUrl, string rewardName);
    }


}