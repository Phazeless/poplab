using System;
using UnityEngine;


namespace RedRain.Poplab.View
{


    /// <summary>
    /// All ui views should inherit this class (or any of it's child interfaces) in order for
    /// Poplab controllers to properly control page transitions.
    /// </summary>
    public interface IPageView
    {
        GameObject gameObject { get; }


        /// <summary>
        /// To be called by Poplab controller, generically instantiates OR enables the game object in scene.
        /// </summary>
        /// <param name="onTransitionComplete">Callback after any transition animation when page is enabled.</param>
        void ShowPage(Action onTransitionComplete = null);


        /// <summary>
        /// To be called by Poplab controller, generically destroys OR hides the game object in scene.
        /// </summary>
        /// <param name="onTransitionComplete">Callback after any transition animation when page is disabled.</param>
        void HidePage(Action onTransitionComplete = null);
    }

}