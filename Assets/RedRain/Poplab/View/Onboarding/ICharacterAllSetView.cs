using System;


namespace RedRain.Poplab.View.Onboarding
{

    public interface ICharacterAllSetView : IPageView
    {

        /// <summary>
        /// Invoked when user clicks the Back button (subscribed by Poplab Controller).
        /// </summary>
        event Action onClickBack;

        /// <summary>
        /// Invoked when user clicks the Launch button (subscribed by Poplab Controller).
        /// </summary>
        event Action onClickLaunch;
    }

}