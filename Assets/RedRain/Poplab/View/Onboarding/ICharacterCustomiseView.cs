using System;


namespace RedRain.Poplab.View.Onboarding
{

    public interface ICharacterCustomiseView : IPageView
    {

        /// <summary>
        /// Invoked when user clicks the Back button (subscribed by Poplab Controller).
        /// </summary>
        event Action onClickBack;
    }

}