using System;


namespace RedRain.Poplab.View.Onboarding
{

    /// <summary>
    /// View class for Character Name page.
    /// </summary>
    public interface ICharacterNameView : IPageView
    {


        #region Input Events

        /// <summary>
        /// Invoked when user selects Male or Female avatar (subscribed by Poplab Controller).
        /// </summary>
        event Action<string> onInputChanged;

        /// <summary>
        /// Invoked when user clicks the Back button (subscribed by Poplab Controller).
        /// </summary>
        event Action onClickBack;

        /// <summary>
        /// Invoked when user clicks the Next button (subscribed by Poplab Controller).
        /// </summary>
        event Action onClickNext;

        #endregion


        /// <summary>
        /// Triggered by Poplab controller.
        /// </summary>
        /// <param name="errors">The list of errors that will be shown below the input field. Null will hide all errors.</param>
        void ShowRegexErrors(string[] errors = null);


        /// <summary>
        /// Triggered by Poplab controller.
        /// </summary>
        /// <param name="error">The error message that will appear over the input field. Null will hide the error.</param>
        void ShowSubmitError(string error = null);


        /// <summary>
        /// Controller to decide when to make the button interactable or not.
        /// </summary>
        void SetNextButtonInteractable(bool isInteractable);
    }

}