using System;


namespace RedRain.Poplab.View.Onboarding
{


    /// <summary>
    /// View class for Choose You Character page.
    /// </summary>
    public interface ICharacterSelectionView : IPageView
    {


        #region Input Events

        /// <summary>
        /// Invoked when user selects Male or Female avatar (subscribed by Poplab Controller).
        /// Return -1 if no skin is selected.
        /// </summary>
        event Action<int> onSelectSkin;

        /// <summary>
        /// Invoked when user clicks the Next button (subscribed by Poplab Controller).
        /// </summary>
        event Action onClickNext;

        #endregion


        int activeIndex { get; }


        /// <summary>
        /// Triggered by Poplab controller.
        /// </summary>
        /// <param name="index">The skin index to highlight in ui. "-1" will not highlight any skin.</param>
        void HighlightSkin(int index);


        /// <summary>
        /// Controller to decide when to make the button interactable or not.
        /// </summary>
        void SetNextButtonInteractable(bool isInteractable);
    }


}