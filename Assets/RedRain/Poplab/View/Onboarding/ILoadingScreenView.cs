namespace RedRain.Poplab.View.Onboarding
{

    public interface ILoadingScreenView : IPageView
    {
        /// <summary>
        /// Will show progress bar once this method is called.
        /// </summary>
        /// <param name="progress">Normalised value btw 0 and 1.</param>
        /// <param name="message">Optional message that replaces the default "Loading..." text.</param>
        void UpdateProgress(float progress, string message = null);
    }

}