using System;


namespace RedRain.Poplab.View.Onboarding
{

    public interface IOnboardSigninView : IPageView
    {


        #region Input Events

        event Action<string> onEmailChanged;
        event Action<string> onPasswordChanged;

        event Action<bool> onTogglePasswordConceal;

        event Action onClickLoginButton;
        event Action onClickForgotPassword;
        event Action onClickSignup;

        // NOTE: not supporting weibo, qq yet 
        event Action onClickAppleLogin;
        event Action onClickFacebookLogin;
        event Action onClickGoogleLogin;
        event Action onClickWechatLogin;

        #endregion


        /// <summary>
        /// Toggles btw ASCII characters and concealed display.
        /// </summary>
        void ConcealPassword(bool isConcealed);


        void ShowError(string message);


        /// <summary>
        /// Controller to decide when to make the button interactable or not.
        /// </summary>
        void SetSignInButtonInteractable(bool isInteractable);

    }

}