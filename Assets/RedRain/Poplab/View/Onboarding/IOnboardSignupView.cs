using System;


namespace RedRain.Poplab.View.Onboarding
{

    public interface IOnboardSignupView : IPageView
    {


        #region Input Events

        event Action<string> onFullNameChanged;
        event Action<string> onEmailChanged;
        event Action<string> onPasswordChanged;
        event Action<string> onConfirmPasswordChanged;

        event Action<bool> onTogglePasswordConceal;
        event Action<bool> onToggleConfirmPasswordConceal;

        event Action onClickSignUpButton;
        event Action onClickSignIn;
        event Action onClickTermsAndCond;
        event Action onClickPrivacyPolicy;

        // NOTE: not supporting weibo, qq yet 
        event Action onClickAppleSignup;
        event Action onClickFacebookSignup;
        event Action onClickGoogleSignup;
        event Action onClickWechatSignup;

        #endregion


        /// <summary>
        /// Toggles btw ASCII characters and concealed display.
        /// </summary>
        void ConcealPassword(bool isConcealed);


        /// <summary>
        /// Toggles btw ASCII characters and concealed display.
        /// </summary>
        void ConcealConfirmPassword(bool isConcealed);


        /// <summary>
        /// Triggered by Poplab controller.
        /// </summary>
        /// <param name="errors">The list of errors that will be shown below the input field. Null will hide all errors.</param>
        void ShowRegexErrors(string[] errors = null);


        /// <summary>
        /// Controller to decide when to make the button interactable or not.
        /// </summary>
        void SetSignUpButtonInteractable(bool isInteractable);

    }

}