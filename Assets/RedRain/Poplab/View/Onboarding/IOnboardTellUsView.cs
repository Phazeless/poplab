using System;
using UnityEngine;


namespace RedRain.Poplab.View.Onboarding
{

    public interface IOnboardTellUsView : IPageView
    {


        #region Input Events

        event Action onClickLike;
        event Action onClickDislike;

        #endregion


        void SetTitle(string title);
        void SetSubtitle(string subtitle);
        void SetImage(Sprite sprite, string subtext);
        void AnimateRight();
        void AnimateLeft();
    }

}