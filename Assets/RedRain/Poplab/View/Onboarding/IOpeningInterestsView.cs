using System;


namespace RedRain.Poplab.View.Onboarding
{

    public interface IOpeningInterestsView : IPageView
    {


        #region Input Events

        /// <summary>
        /// Invoked when user either clicks like/dislike OR swipes left/right on current interest.
        /// To pass the interest index, and True if user liked the interest, False if disliked.
        /// </summary>
        event Action<int, bool> onInterestEvent;

        #endregion


        /// <summary>
        /// Used by controller to add interests for users to choose.
        /// First interest appear on top, subsequent ones below.
        /// </summary>
        /// <param name="index">Identifier index set by controller to differentiate the interests.</param>
        /// <param name="title">Title that appears at bottom of image.</param>
        /// <param name="imageUrl">The url of image to be downloaded by view to display.</param>
        void AddInterestToStack(int index, string title, string imageUrl);


        /// <summary>
        /// Triggered by controller when it receives the onInterestEvent.
        /// </summary>
        void AnimateSwipeLeft();


        /// <summary>
        /// Triggered by controller when it receives the onInterestEvent.
        /// </summary>
        void AnimateSwipeRight();
    }

}