using System;


namespace RedRain.Poplab.View.Onboarding
{

    /// <summary>
    /// View class that controls the opening pages for new users. (i.e. Exclusive Lab Access, Into the Metaverse, Game on, loot up)
    /// </summary>
    public interface IOpeningOnboardView : IPageView
    {


        #region Input Events

        /// <summary>
        /// Invoked when relevant user gesture is detected (subscribed by Poplab Controller).
        /// </summary>
        event Action onNext;

        /// <summary>
        /// Invoked when relevant user gesture is detected (subscribed by Poplab Controller).
        /// </summary>
        event Action onPrevious;

        #endregion


        /// <summary>
        /// Triggered by Poplab controller.
        /// </summary>
        /// <param name="index">The index to show.</param>
        void ShowIndex(int index);
    }

}