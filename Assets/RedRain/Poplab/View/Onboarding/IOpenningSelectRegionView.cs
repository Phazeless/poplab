using System;


namespace RedRain.Poplab.View.Onboarding
{

    public interface IOpenningSelectRegionView : IPageView
    {


        #region Input Events

        event Action<int> onIndexChanged;
        event Action onNext;

        #endregion


        int activeIndex { get; }


        /// <summary>
        /// Controller to decide when to make the button interactable or not.
        /// </summary>
        void SetNextButtonInteractable(bool isInteractable);

    }

}