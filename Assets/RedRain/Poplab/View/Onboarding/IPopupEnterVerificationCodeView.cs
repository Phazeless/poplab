using System;


namespace RedRain.Poplab.View.Onboarding
{


    public interface IPopupEnterVerificationCodeView : IPageView
    {


        #region Input Events

        event Action<int> onChar1Changed;
        event Action<int> onChar2Changed;
        event Action<int> onChar3Changed;
        event Action<int> onChar4Changed;
        event Action<int> onChar5Changed;
        event Action<int> onChar6Changed;

        event Action onClickResendCode;
        event Action onClickContinue;
        event Action onClickBack;

        #endregion


        /// <param name="isError">if True, display message in red.</param>
        void ShowLabel(string message, bool isError = false);


        /// <summary>
        /// Controller to decide whether to show/hide Resend Code button.
        /// </summary>
        void ShowHideResendCodeButton(bool show);


        /// <summary>
        /// Controller to decide when to make the Continue button interactable or not.
        /// </summary>
        void SetContinueButtonInteractable(bool isInteractable);

    }

}