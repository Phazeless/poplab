using System;


namespace RedRain.Poplab.View.Onboarding
{


    public interface IPopupForgotPasswordView : IPageView
    {


        #region Input Events

        event Action<string> onEmailChanged;

        event Action onClickContinue;
        event Action onClickBack;

        #endregion


        void ShowError(string message);


        /// <summary>
        /// Controller to decide when to make the button interactable or not.
        /// </summary>
        void SetContinueButtonInteractable(bool isInteractable);
    }

}