using System;


namespace RedRain.Poplab.View.Onboarding
{

    public interface IPopupResetPasswordView : IPageView
    {


        #region Input Events

        event Action<string> onPasswordChanged;
        event Action<string> onConfirmPasswordChanged;

        event Action<bool> onTogglePasswordConceal;
        event Action<bool> onToggleConfirmPasswordConceal;

        event Action onClickReset;
        event Action onClickBack;

        #endregion


        /// <summary>
        /// Toggles btw ASCII characters and concealed display.
        /// </summary>
        void ConcealPassword(bool isConcealed);


        /// <summary>
        /// Toggles btw ASCII characters and concealed display.
        /// </summary>
        void ConcealConfirmPassword(bool isConcealed);


        void ShowError(string message);


        /// <summary>
        /// Controller to decide when to make the Continue button interactable or not.
        /// </summary>
        void SetResetPasswordButtonInteractable(bool isInteractable);
    }

}