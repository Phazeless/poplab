using System;


namespace RedRain.Poplab.View.Quest
{

    /// <summary>
    /// View class for quest page.
    /// </summary>
    public interface IQuestPageView : IPageView
    {


        #region Input Events

        /// <summary>
        /// Invoked when user clicks on the daily rewards button (at top right corner of page).
        /// </summary>
        event Action onClickDailyRewards;

        /// <summary>
        /// Invoked when user clicks on back button.
        /// </summary>
        event Action onClickBack;

        #endregion


        /// <summary>
        /// Creates a new row of quest info (from scene template) on the quest page.
        /// </summary>
        IQuestRowView CreateNewQuestRow();


        /// <summary>
        /// Creates a new row of task info (from scene template) on the quest page.
        /// </summary>
        ITaskRowView CreateNewTaskRow();


        /// <summary>
        /// Creates a new row of reward info (from scene template) on the quest page.
        /// </summary>
        IRewardRowView CreateNewRewardRow();
    }

}