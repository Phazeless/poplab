using System;


namespace RedRain.Poplab.View.Quest
{

    /// <summary>
    /// View of a row of quest info in the quest page view, to be configured by controller.
    /// </summary>
    public interface IQuestRowView
    {


        #region Input Events

        /// <summary>
        /// Invoked when user clicks on the arrow of a specific quest id (event is sent to controller).
        /// </summary>
        event Action<long> onClickArrow;

        #endregion


        /// <summary>
        /// Sets the display info of the quest record.
        /// </summary>
        /// <param name="questId">The (hidden) id of the quest to be passed when arrow is toggled.</param>
        /// <param name="displayName">Name of the quest that user sees.</param>
        /// <param name="displayExpiryInfo">Expiry info of the quest that user sees.</param>
        /// <param name="imageUrl">Url of quest image/icon that user sees (i.e. merchant logo).</param>
        void SetInfo(long questId, string displayName, string displayExpiryInfo, string imageUrl);


        /// <summary>
        /// Sets the direction of the (show/hide) arrow.
        /// </summary>
        /// <param name="isUp">If true, the arrow should be pointing up. Vice versa.</param>
        void UpdateArrowDirection(bool isUp);
    }

}