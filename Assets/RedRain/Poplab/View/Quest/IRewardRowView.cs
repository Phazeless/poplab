namespace RedRain.Poplab.View.Quest
{

    /// <summary>
    /// View of a row of reward info in the quest page view, to be configured by controller.
    /// </summary>
    public interface IRewardRowView
    {
        /// <summary>
        /// Adds a pair of reward image + quantity to the row display. If quantity = 1, can hide the quantity display from UI.
        /// </summary>
        /// <param name="imageUrl"></param>
        /// <param name="rewardQty"></param>
        void AddRewardItem(string imageUrl, int rewardQty = 1);


        /// <summary>
        /// Toggles between the claimed and unclaimed view states for the whole row.
        /// </summary>
        /// <param name="isClaimed">Pass true if reward has been claimed.</param>
        void SetRewardState(bool isClaimed);
    }

}