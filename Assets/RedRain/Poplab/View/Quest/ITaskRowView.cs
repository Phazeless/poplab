using System;


namespace RedRain.Poplab.View.Quest
{

    /// <summary>
    /// View of a row of task info in the quest page view, to be configured by controller.
    /// </summary>
    public interface ITaskRowView
    {


        #region Input Events

        /// <summary>
        /// Invoked when user clicks on Go button of the specific task id (event is sent to controller).
        /// </summary>
        event Action<long> onClickGo;

        #endregion


        /// <summary>
        /// Sets the display info of the task record.
        /// </summary>
        /// <param name="taskId">The (hidden) id of the task to be passed when Go button is clicked.</param>
        /// <param name="displayName">Name of the task that user sees.</param>
        /// <param name="currProgress">User's current progress of this task.</param>
        /// <param name="totalProgress">Total progress to complete this task.</param>
        /// <param name="rewardImageUrl">Url or reward image/icon that user sees.</param>
        /// <param name="rewardQty">Qty of the reward to be earned on completing task.</param>
        /// <param name="isGoBtnInteractable">Bool that decides if Go button is clickable or not.</param>
        void SetInfo(long taskId,
                     string displayName,
                     int currProgress,
                     int totalProgress,
                     string rewardImageUrl,
                     int rewardQty,
                     bool isGoBtnInteractable);


        /// <summary>
        /// Sets the height of the task record. Used to control the collapse/expansion of each task record.
        /// </summary>
        /// <param name="normalisedSize">Y-scale value between 0 and 1.</param>
        void SetHeight(float normalisedSize);
    }

}